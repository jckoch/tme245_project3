function [KMe, fe, KGe] = shell2rg(ex, ey, ep, D, G, eq, es)
%function [KMe, fe, KGe] = shell2rg(ex, ey, ep, D, G, eq, es)
%
% Input:
% ex [nelm, nnodes]     X-coordinates
% ey [nelm, nnodes]     Y-coordinates
% ep [1, 4]             Vector of four, [ptype, t, irb, irs], where 
%                       ptype=1 gives plane stress and ptype=2 gives plane 
%                       strain, t is the thickness of the plate. irb and 
%                       irs are the integration rules for bending and 
%                       shearing ,respectively, (only for Mindlin plate).
% D  [3, 3]             Constitutive matrix% ex: X-coordinates
% eq [3, 1]             Vector of three, [bx, by, qz] containing the
%                       external element loading
% es [nelm, 3 or 4]     element stresses (3 columns if sigz is zero
%                       otherwise 4 columns)
%
% Output:
% KGe [20, 20]          element geometrical stiffness matrix
% fe  [20, 1]           element load vector
% KMe [20, 20]          element material stiffness matrix
%
%--------------------------------------------------------------------------
% Copyright (C) 2018 James Koch and Amer Bitar
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License v2 as published by
% the Free Software Foundation.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%--------------------------------------------------------------------------

% constitutive matrix
Dmat = D;
Gmat = G;

% allow only Kirchoff plate theory
plate = "Kirchoff";

% define bending degrees of freedom
v3 = 3:5:20;
v4 = 4:5:20;
v5 = 5:5:20;
N = 4;
ia = 1+rem(0:N-1, numel(v3));
ib = 1+rem(0:N-1, numel(v4));
ic = 1+rem(0:N-1, numel(v5));
plate_ind = [v3(ia); v4(ib); v5(ic)];
plate_ind = reshape(plate_ind,1,[]);

% element material stiffness matrix
[KMe, fe] = shell2re(ex, ey, ep, Dmat, Gmat, eq, plate);

% plate thickness
t = ep(2);

% membrane streses
sig = [es(1) es(4);
       es(4) es(2)];

% compute length of element
Lx=ex(3)-ex(1); Ly=ey(3)-ey(1);
a = (1/2)*Lx; b = (1/2)*Ly;

% compute the scaled gauss points
GaussPointsX = (sqrt(3/5))*[-1; -0; 1; -1; 0; 1; -1; 0; 1];
GaussPointsY = (sqrt(3/5))*[1; 1; 1; 0; 0; 0; -1; -1; -1];
ngp = length(GaussPointsX);

% compute the scaled integration weights
intWeightX = (1/9)*[5; 8; 5; 8; 5; 8; 5; 8; 5];
intWeightY = (1/9)*[5; 5; 5; 8; 8; 8; 5; 5; 5];

% initialize geometric stiffness matrix
KGe = zeros(size(KMe));
KGe_plate = zeros(size(KMe(plate_ind, plate_ind)));

% for loop
for gpIndex = 1:ngp
    
    % Gauss point for each loop iteration (CORRECT)
    xsi       = GaussPointsX(gpIndex)*a; 
    weightXsi = intWeightX(gpIndex)*a;
    eta       = GaussPointsY(gpIndex)*b;
    weightEta = intWeightY(gpIndex)*b;
    
    % derivatives of shape functions (CORRECT)
    dN = [0, 1, 0, 2*xsi, eta, 0, 3*xsi^2, 2*xsi*eta, eta^2, 0, 3*xsi^2*eta, eta^3;
          0, 0, 1, 0, xsi, 2*eta, 0, xsi^2, 2*xsi*eta, 3*eta^2, xsi^3, 3*xsi*eta^2];
    
    % C-matrix (CORRECT)
    C = [1, -a, -b, a^2, a*b, b^2, -a^3, -a^2*b, -a*b^2, -b^3, a^3*b, a*b^3;
         0, 0, 1, 0, -a, -2*b, 0, a^2, 2*a*b, 3*b^2, -a^3, -3*a*b^2;
         0, -1, 0, 2*a, b, 0, -3*a^2, -2*a*b, -b^2, 0, 3*a^2*b, b^3;
         1, a, -b, a^2, -a*b, b^2, a^3, -a^2*b, a*b^2, -b^3, -a^3*b, -a*b^3;
         0, 0, 1, 0, a, -2*b, 0, a^2, -2*a*b, 3*b^2, a^3, 3*a*b^2;
         0, -1, 0, -2*a, b, 0, -3*a^2, 2*a*b, -b^2, 0, 3*a^2*b, b^3;
         1, a, b, a^2, a*b, b^2, a^3, a^2*b, a*b^2, b^3, a^3*b, a*b^3;
         0, 0, 1, 0, a, 2*b, 0, a^2, 2*a*b, 3*b^2, a^3, 3*a*b^2;
         0, -1, 0, -2*a, -b, 0, -3*a^2, -2*a*b, -b^2, 0, -3*a^2*b, -b^3;
         1, -a, b, a^2, -a*b, b^2, -a^3, a^2*b, -a*b^2, b^3, -a^3*b, -a*b^3;
         0, 0, 1, 0, -a, 2*b, 0, a^2, -2*a*b, 3*b^2, -a^3, -3*a*b^2;
         0, -1, 0, 2*a, -b, 0, -3*a^2, 2*a*b, -b^2, 0, -3*a^2*b, -b^3];
     
    % compute B 
    B = dN*inv(C);    
    
    % compute Ke
    KGe_plate = KGe_plate + B'*t*sig*B*weightXsi*weightEta;
end

KGe(plate_ind, plate_ind) = KGe_plate;

end