function []=Dispviz3(ex,ey,ez,edof,a,Ddof,plotpar)
% []=Dispviz3(ex,ey,ez,edof,a,Ddof,plotpar)
%----------------------------------------------------------------------
%  PURPOSE
%  Interpolates values from the vector a into the element mesh
%  defined by ex, ey and ez using edof topology
%
%  INPUT:  ex   = [x1 x2 x3 ... xn] 
%          ey   = [y1 y2 y3 ... yn]           element coordinates
%          ez   = [z1 z2 z3 ... zn] 
%
%          edof = [el_nr n1 n2 .. nn]         topology matrix
%
%          a    = [a1 a2 a3 a4 ... an]        displacement vector
%
%          Ddof = [d.o.f]                     Ddof:  1=for x disp
%                                                           2=for y disp
%                                                           3=for z disp
%                                                           4=for x rot
%                                                           5=for y rot
%
%          plotpar = [colorbar, cmin, cmax]   colbar = 1 -> yes
%                                                      2 -> no
%                                             case colorbar = 1
%                                                  cmin : axis scaling min
%                                                  cmax : axis scaling max
%                                                  cmin=cmax=>automatic scale
%
%  OUTPUT: Colorplot
%---------------------------------------------------------------------

% WRITTEN BY   : J Eriksson 2002-04-16 
% LAST MODIFIED: J Eriksson 2002-11-16
% Copyright (c)  Department of Structural Mechanics
%                Chalmers University of Technology
%----------------------------------------------------------------------

hold on;
axis equal;
NEL=size(edof);
Szedof=size(edof);
flag=plotpar(1);
Ndof=NEL(2)-1;
Nnodes=size(ex);
Dof=Ndof/Nnodes(2);
DofTimes=Ndof/Dof;

if flag==1  
    cmin=plotpar(2);cmax=plotpar(3);
end

for i=1:NEL(1)
    for ii=1:DofTimes
        %index((1:length(Ddof))+(ii-1)*length(Ddof))=[Ddof+(ii-1)*Dof+1];
        z(1,ii)=sum(a(edof(i,[Ddof+(ii-1)*Dof]+1)));
    end
   	patch(ex(i,:)',ey(i,:)',ez(i,:)',z');
end
axis image

if (flag==1) & (cmin~=cmax)
    caxis([cmin cmax]);
    colorbar
elseif flag==1
    colorbar
end
hold off;
view([0 0 1])

end
% ---------------------------- end -----------------------------