function [es, et, ef, ec] = shell2rs(ex, ey, ep, Dmat, ed)
%function [es, et, ef, ec] = shell2rs(ex, ey, ep, Dmat, ed)
% 
% Input:
% ex [nelm, nnodes]     X-coordinates
% ey [nelm, nnodes]     Y-coordinates
% ep [1, 4]             Vector of four, [ptype, t, irb, irs], where 
%                       ptype=1 gives plane stress and ptype=2 gives plane 
%                       strain, t is the thickness of the plate. irb and 
%                       irs are the integration rules for bending and 
%                       shearing ,respectively, (only for Mindlin plate).
% D  [3, 3]             Constitutive matrix
% ed [nelm, dof]        element displacements for each dof
%
% Output:
% es [nelm, 3 or 4]     element stresses (3 columns if sigz is zero
%                       otherwise 4 columns)
% et [nelm, 3 or 4]     element strains (3 columns if sigz is zero
%                       otherwise 4 columns)
% ef [nelm, 5]          element forces
% ec [nelm, 3]          element curvatures
%
%--------------------------------------------------------------------------
% Copyright (C) 2018 James Koch and Amer Bitar
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License v2 as published by
% the Free Software Foundation.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%--------------------------------------------------------------------------

% element normal and shear stress 
D=Dmat; % define D constitutive matrix

% define in-plane degrees of freedom
v1 = 1:5:20;
v2 = 2:5:20;
N = max(numel(v1), numel(v2));
ia = 1+rem(0:N-1, numel(v1));
ib = 1+rem(0:N-1, numel(v2));
plane_ind = [v1(ia); v2(ib)];
plane_ind = reshape(plane_ind,1,[]);

% define bending degrees of freedom
v3 = 3:5:20;
v4 = 4:5:20;
v5 = 5:5:20;
N = 4;
ia = 1+rem(0:N-1, numel(v3));
ib = 1+rem(0:N-1, numel(v4));
ic = 1+rem(0:N-1, numel(v5));
plate_ind = [v3(ia); v4(ib); v5(ic)];
plate_ind = reshape(plate_ind,1,[]);

% define in-plane x- and y-coordinates (i.e. only need 2 points of the
% 4-node element which are diagonals to each other; does not matter which
% diagonal)
ex_plane = ex(1:2:end);
ey_plane = ey(1:2:end);

% the order of the plane strains in the element total stains matrix
ed_plan = ed(plane_ind);  

% the order of the plate strains in the element total stains matrix
ed_plat = ed(plate_ind);

% compute element stresses and strains
[es, et] = planrs(ex_plane, ey_plane, ep(1:2), D, ed_plan);

% compute element forces and curvatures
[ef, ec] = platrs(ex, ey, ep(2), D, ed_plat);

end