function [u, ed, Q, stress, strain, forces, curvat, pstress] = solve_2Dplate(Coord, Edof, Ex, Ey, DofTop, E, nu, t_y, bc, ep, eq, plate)
%[u, ed, stress, strain, forces, curvat, cdefl] = solve_2Dplate(Edof, Ex, Ey, E, nu, ep, eq, plate)
%
%--------------------------------------------------------------------------
% PURPOSE
% Implements the FE-method in the solution of 2-dimensional plates
% according to either Kirchoff or Mindlin theory. The solution algorithm
% utilizes a plane shell element to compute the unknown degrees of freedom
% (i.e. displacement and rotations) as well as post-processing to determine
% the reaction forces/moments, stresses, strains, and sectional
% forces/moments.
% 
% Inputs:
% Coord   [NoNodes, 2]      Nodal coordinates matrix
% Edof    [nelm, 21]        Topology matrix in terms of dofs
%                           In each row in Edof, the dofs are arranged 
%                           anti-clockwise starting from the bottom left node
%                           of each element (5 dofs/node). For each node, first
%                           the three translational dofs are counted (ux,uy,uz) 
%                           and then the two rotational (theta_x,theta_y)
% Ex      [nelm, 4]         Element nodal x-coordinates, where the nodes are
%                           counted anti-clockwise starting from the bottom left
%                           node of each element.
% Ey      [nelm, 4]         Element nodal y-coordinates, where the nodes are
%                           counted anti-clockwise starting from the bottom left
%                           node of each element.
% DofTop  [3*nx+1, 1]       Translational dofs (ux,uy,uz) at the right boundary,
%                           size = (3xNoNodesRightBoundary, 1)
% E       [scalar]          modulus of elasticity
% nu      [scalar]          Poisson's ratio
% t_y     [scalar]          in-plane boundary load on top boundary
% bc      [varies]          boundary conditions  
% ep      [1, 4]            Vector of four, [ptype, t, irb, irs], where 
%                           ptype=1 gives plane stress and ptype=2 gives plane 
%                           strain, t is the thickness of the plate. irb and 
%                           irs are the integration rules for bending and 
%                           shearing ,respectively, (only for Mindlin plate).    
% eq      [3, 1]            Vector of three, [bx, by, qz] containing the
%                           external element loading
% plate   [scalar]          1 for Kirchhoff plate and 2 for Mindlin plate
%
% Outputs:
% u       [ndof, 1]         the solution in terms of each DOF
% ed      [nelm, ndof]      displacements per element (one row per element)
% Q       [ndof, 1]         reaction forces/moments in one vector
% stress  [nelm, 4(3)]      element stresses (3 columns if sigz is zero)
% strain  [nelm, 4(3)]      element strains (3 columns if strain in z is zero)
% forces  [nelm, 5]         element forces
% curvat  [nelm, 3]         element curvatures
% pstress [nelm, 1]
%
%--------------------------------------------------------------------------
% Copyright (C) 2018 James Koch and Amer Bitar
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License v2 as published by
% the Free Software Foundation.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%--------------------------------------------------------------------------                         

%% define FE method specific properties
% define constitutive matrices
Dmat = hooke(ep(1), E, nu);
Gmat = E/(2*(1 + nu))*[1 0;
                       0 1];

% initialize matrices
ndof = max(Edof(:));
N = 20;
K = spalloc(ndof, ndof, N*ndof);
f = zeros(ndof, 1);

% call element routine
for i = 1:size(Edof, 1)
    [Ke, fe] = shell2re(Ex(i, :), Ey(i, :), ep, Dmat, Gmat, eq, plate);
    K(Edof(i, 2:end), Edof(i, 2:end)) = K(Edof(i, 2:end), Edof(i, 2:end)) + Ke;
    f(Edof(i, 2:end)) = f(Edof(i, 2:end)) + fe;
end

%% boundary load vector for the in-plane problem
fb = zeros(ndof, 1);
DofTop_y = DofTop(2:3:end);     
numTopNodes = size(DofTop_y, 1) - 1;
t = ep(2); % thickness of the plate

for i = 1:numTopNodes
    coordElm = [Coord((DofTop_y(i) + 3)/5, :);
                Coord((DofTop_y(i+1) + 3)/5, :)];
    dx = coordElm(1, 1) - coordElm(2, 1);
    dy = coordElm(1, 2) - coordElm(2, 2);
    Le = sqrt(dx^2 + dy^2);    
    
    fbe = [0;
          (Le*t_y*t)/2; 
          0;
          (Le*t_y*t)/2];
    
    dof_ind = [DofTop_y(i) - 1, DofTop_y(i), ...
               DofTop_y(i+1) - 1, DofTop_y(i+1)];

    fb(dof_ind) = fb(dof_ind) + fbe;    
end

%% solve the system of equations
[u, Q] = solveq(K, f+fb, bc);

%% post-processing to determine stress, strains, sectional forces/moments
% initialize global stress, strain, moment/force, and curvature matrices
stress = zeros(size(Edof, 1), 4);
strain = zeros(size(Edof, 1), 4);
forces = zeros(size(Edof, 1), 5);
curvat = zeros(size(Edof, 1), 3);

% compute element displacements (using extract from CALFEM)
ed = extract(Edof, u);

% for loop
for i = 1:size(Edof, 1)
    [es, et, ef, ec] = shell2rs(Ex(i, :), Ey(i, :), ep, Dmat, ed(i, :));
    
    if size(es, 2) == 3
        stress(i, 1:2) = es(:, 1:2);
        stress(i, 4) = es(:, 3);
        strain(i, 1:2) = et(:, 1:2);
        strain(i, 4) = et(:, 3);
    else
        stress(i, :) = es;
        strain(i, :) = et;
    end
    forces(i, :) = ef;
    curvat(i, :) = ec;
    
    
    % compute the principal stresses using principal component analysis
    sigma = [es(1), es(3);
             es(3), es(2)];
    pstress(i, :) = min(eig(sigma));
end

end