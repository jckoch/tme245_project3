% Project 3
% Authors: James Koch and Amer Bitar
%
%--------------------------------------------------------------------------
% PURPOSE
% To solve the specific problem using the FE-method described in the task
% description in the file "TME245_CA3.pdf" as part of the course "TME245 -
% Finite element structures" at Chalmers University of Technolgy.
%
%--------------------------------------------------------------------------
% Copyright (C) 2018 James Koch and Amer Bitar
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License v2 as published by
% the Free Software Foundation.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%--------------------------------------------------------------------------

close all;
clear all; clc;
set(0, 'DefaultAxesFontSize', 16); %change the font size of axis in figures

%% define general problem specific properties
% geometrical
Lx = 3.0;                               % [m], length
Ly = 1.0;                               % [m], width
t = 0.008;                              % [m], thickness

% material constants
E = 210e9;                              % [Pa], modulus of elasticity
nu = 0.3;                               % [-], Poisson's ratio

% external loading
t_y = -5000000;                         % [N/m^2], in-plane load
q_z = -1000;                            % [N/m^2], out-of-plane load

%% define FE-mesh properties (specific to given problem)
% number of elements in each direction
% NOTE: choose even number of elements in x- and y-direction to get
% translational DOFs at the center of the shell element
nx = 18*2;
ny = 6*2;

% plot the mesh?
plotMesh = 'yes';

% generate the FE-mesh
[Coord, Dof, Enode, Edof, Ex, Ey, DofRight, DofTop, DofLeft, DofBottom, ...
    cdof] = quadmesh(Lx, Ly, nx, ny, plotMesh);

%% define boundary conditions
bc_outofplane_disp = [DofTop(3:3:end);
                      DofRight(3:3:end);
                      DofBottom(3:3:end);
                      DofLeft(3:3:end)];
bc_inplane_disp = [DofRight(1:3:end); DofRight(2:3:end); 
                   DofLeft(1:3:end); DofLeft(2:3:end)];
bc = [bc_inplane_disp;
      bc_outofplane_disp];
bc(:, 2) = zeros(size(bc(:, 1)));

%% define finite element properties specific to given problem
% define element properties
ptype = 1;          % 1=plane stress; % 2=plane strain
irb = 3; irs = 3;   % integration rule (only for Mindlin plate)
ep = [ptype t irb irs];

%% define external element loading for the given problem
bx = 0;
by = 0;
qz = q_z;
eq = [bx by qz];

%% Stress analysis: Task 1
% FE solution using Kirchoff plate theory
plate = "Kirchoff";

% run the FE-solver for a 2D plate using Kirchoff plate theory
[u, ed, Q, stress, strain, forces, curvat, pstress] = solve_2Dplate(Coord, Edof, Ex, Ey, DofTop, E, nu, t_y, bc, ep, eq, plate);

% prep some variables for plotting
Ez = zeros(size(Ex));
cmin = 1; cmax = 1;
plotpar = [1, cmin, cmax];

%Subtask 1b: draw deformed and undeformed mesh
Ddof = [1 2 3 4 5];
symdisp = ["x_disp", "y_disp", "z_disp", "x_rot", "y_rot"];
titles = ["Displacement in the x-direction", ...
          "Displacement in the y-direction", ...
          "Displacement in the z-direction", ...
          "Rotation in the x-direction", ...
          "Rotation in the y-direction"];      
kk = 2; % figure counter
for i = 1:length(Ddof)
    figure(kk); axis equal;
    colormap jet;
    Dispviz3(Ex, Ey, Ez, Edof, u, Ddof(i), plotpar);
    title(titles(i));
    saveas(kk, sprintf('imgs/%s', symdisp(i)), 'epsc');
    kk = kk + 1;
end

% Subtask 1c
% Plot the moments: Mxx, Myy, and Mxy
symMoment = ["M_{xx}", "M_{yy}", "M_{xy}"];
minMoment = min(forces(:, 1:3), [], 'all');
maxMoment = max(forces(:, 1:3), [], 'all');
for i = 1:(size(forces, 2) - 2)
    figure(kk);
    hold on; axis image;
    c = colorbar;
    fill(Ex', Ey', forces(:, i));
    colormap jet;
    caxis([minMoment, maxMoment]);
    c.Label.String = 'Moment [N m]';
    c.Label.FontSize = 16;
    title(symMoment(i));
    saveas(kk, sprintf('imgs/%s', symMoment(i)), 'epsc');
    kk = kk + 1;
end

% Plot the mean stresses: sigxx, sigyy, and sigxy and minimum principal
% stress (i.e. compression)
symsig = ["sig_xx", "sig_yy", "sig_zz", "sig_xy", "sig_principal_compressive"];
titles = ["\sigma_{xx}", "\sigma_{yy}", "\sigma_{zz}", "\sigma_{xy}", "Minimum principal stress"];
minStress = min(stress(:));
maxStress = max(stress(:));
for i = 1:size(symsig, 2)
    figure(kk);
    hold on; axis image;
    c = colorbar;
    colormap jet;
    if i <= 2 || i == 4
        fill(Ex', Ey', stress(:, i));
        caxis([minStress, maxStress]);
        c.Label.String = 'Stress [Pa]';
        c.Label.FontSize = 16;
        title(titles(i));
        saveas(kk, sprintf('imgs/%s', symsig(i)), 'epsc');
        kk = kk + 1;
    elseif i == 5
        fill(Ex', Ey', pstress(:, 1));
        caxis([minStress, maxStress]);
        c.Label.String = 'Stress [Pa]';
        c.Label.FontSize = 16;
        title(titles(i));
        saveas(kk, sprintf('imgs/%s', symsig(i)), 'epsc');
        kk = kk + 1;
    end
end

%% Stress analysis: Task 2
% FE solution using both Kirchoff and Mindlin plate theory
plate = ["Kirchoff", "Mindlin", "Mindlin"];

% set thickness of the plate as variable from 0.001m to 0.5m
t = linspace(0.001, 0.5, 50);
irb = [2, 2, 2];
irs = [2, 2, 1];

% initialize variables
scaleddefl = zeros(size(t, 2), 1);
SCALEDDEFL = zeros(size(t, 2), size(irb, 2));

%for loop
for p = 1:length(irb)
    for i = 1:length(t)
        ep = [ptype t(i) irb(p) irs(p)];
        [u, ~, ~, ~, ~, ~, ~] = solve_2Dplate(Coord, Edof, Ex, Ey, DofTop, E, nu, t_y, bc, ep, eq, plate(p));
        
        % extract the deflection of the center-most point
        scaleddefl(i, :) = u(cdof(3))*t(i)^3;
    end
    SCALEDDEFL(:, p) = scaleddefl;
end

% plot the deflection as a function of plate thickness
figure(kk);
hold on;
plot(t, SCALEDDEFL(:, 1)); % Kirchoff
plot(t, SCALEDDEFL(:, 2)); % Mindlin with irb=2 and irs=2
plot(t, SCALEDDEFL(:, 3)); % Mindlin with irb=2 and irs=1
legend("Kirchoff", "Mindlin: irs=2", "Mindlin: irs=1", "Location", "northeast", 'FontSize', 20);
xlabel("Plate thickness [m]", 'FontSize', 20);
ylabel("Out-of-plane deflection [m]", 'FontSize', 20);
saveas(kk, 'imgs/kirchoffvmindlin', 'epsc');
hold off;
kk = kk + 1;

%% Linearized pre-buckling analysis: Task 2
% FE solution using Kirchoff plate theory
plate = "Kirchoff";

t = 0.008;
ep(2) = t;

% define D and G
D = hooke(ep(1), E, nu);
G = E/(2*(1 + nu))*[1 0;
                    0 1];

% initialize global material and geometric stiffness matrices
ndof = max(Edof(:)); N = 10;
KM = spalloc(ndof, ndof, N*ndof);
KG = spalloc(ndof, ndof, N*ndof);

% for loop
for i = 1:size(Edof, 1)
    % compute material and geometric element stiffness matrices and element
    % load vector
    [KMe, fe, KGe] = shell2rg(Ex(i, :), Ey(i, :), ep, D, G, eq, stress(i, :));
    
    % assemble material global stiffness matrix
    KM(Edof(i, 2:end), Edof(i, 2:end)) = KM(Edof(i, 2:end), Edof(i, 2:end)) + KMe;
    
    % assemble geometric global stiffness matrix
    KG(Edof(i, 2:end), Edof(i, 2:end)) = KG(Edof(i, 2:end), Edof(i, 2:end)) + KGe;
end

% find the free degree of freedom problem
freedof = setdiff(1:(nx+1)*(ny+1)*5, bc(:, 1));
KMff = sparse(KM(freedof, freedof));
KGff = sparse(KG(freedof, freedof));
Keig = KMff\KGff;
[X, L] = eigs(Keig);

% determine the eigenvalues (critical buckling loads)
lambda = -1./diag(L);

% plot the 1st two buckled mode shapes
titles = ["First buckled mode shape",
          "Second buckled mode shape"];
filenames = ["firstbuckledmode";
             "secondbuckledmode"];
for i = 1:2
    ub = zeros(ndof, 1);
    ub(freedof) = X(:, i);
    edb = extract(Edof, ub);
    
    figure(kk);
    hold on; axis image;
    colorbar; 
    fill(Ex', Ey', edb(:, 3:5:20)');
    c.Label.String = 'Out-of-plane displacment [m]';
    c.Label.FontSize = 16;
    title(titles(i));
    colormap jet;
    saveas(kk, sprintf('imgs/%s', filenames(i)), 'epsc');
    kk = kk + 1;
end


