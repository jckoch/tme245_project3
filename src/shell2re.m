function [Ke, fe] = shell2re(ex, ey, ep, Dmat, Gmat, eq, plate)
% function [Ke, fe] = shell2re(ex, ey, ep, Dmat, Gmat, eq, plate)
%
% Input:
% ex [nelm, nnodes]     X-coordinates
% ey [nelm, nnodes]     Y-coordinates
% ep [1, 4]             Vector of four, [ptype, t, irb, irs], where 
%                       ptype=1 gives plane stress and ptype=2 gives plane 
%                       strain, t is the thickness of the plate. irb and 
%                       irs are the integration rules for bending and 
%                       shearing ,respectively, (only for Mindlin plate).
% D  [3, 3]             Constitutive matrix
% G  [2, 2]             Constitutive matrix for bending mode (Mindlin plate)
% eq [3, 1]             Vector of three, [bx, by, qz] containing the
%                       external element loading
% plate [scalar]        1 for Kirchhoff plate and 2 for Mindlin plate
%
% Output:
% Ke [ndof, ndof]       Element stiffness matrix
% fe [ndof, 1]          Element load vector
%
%--------------------------------------------------------------------------
% Copyright (C) 2018 James Koch and Amer Bitar
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License v2 as published by
% the Free Software Foundation.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%--------------------------------------------------------------------------

% define the constitutive matrices
D = Dmat;
G = Gmat;

% define in-plane degrees of freedom
v1 = 1:5:20;
v2 = 2:5:20;
N = max(numel(v1), numel(v2));
ia = 1+rem(0:N-1, numel(v1));
ib = 1+rem(0:N-1, numel(v2));
plane_ind = [v1(ia); v2(ib)];
plane_ind = reshape(plane_ind,1,[]);

% define bending degrees of freedom
v3 = 3:5:20;
v4 = 4:5:20;
v5 = 5:5:20;
N = 4;
ia = 1+rem(0:N-1, numel(v3));
ib = 1+rem(0:N-1, numel(v4));
ic = 1+rem(0:N-1, numel(v5));
plate_ind = [v3(ia); v4(ib); v5(ic)];
plate_ind = reshape(plate_ind,1,[]);

% define in-plane x- and y-coordinates (i.e. only need 2 points of the
% 4-node element which are diagonals to each other; does not matter which
% diagonal)
ex_plane = ex(1:2:end);
ey_plane = ey(1:2:end);

% in-plane contribution
[Ke_plane, fe_plane]=planre(ex_plane, ey_plane, ep(1:2), D, eq(1:2)');

% bending contribution
if strcmp(plate, 'Kirchoff')
    [Ke_plate, fe_plate] = platre(ex, ey, ep(2), D, eq(3));
elseif strcmp(plate, 'Mindlin')
    [Ke_plate, fe_plate] = platme(ex, ey, ep(2:4), D, G, eq(3));
end
    
Ke(plane_ind, plane_ind) = Ke_plane;
Ke(plate_ind, plate_ind) = Ke_plate;

fe(plane_ind, 1) = fe_plane;
fe(plate_ind, 1) = fe_plate;
  
end