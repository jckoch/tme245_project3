function [Ke,fe]=platme(ex,ey,ep,D,G,q)
% Ke=platMe(ex,ey,ep,D,G)
% [Ke,fe]=platMe(ex,ey,ep,D,G,eq)
% -------------------------------------------------------------
%  PURPOSE
%   Calculate the stiffness matrix for a 4-node Mindlin plate element.
%  
%   INPUT:  ex = [x1 x2 x3 x4]      element coordinates
%           ey = [y1 y2 y3 y4]
%  
%           ep=[t, irb, irs]        t-thickness
%                                   irb-integration rule bending
%                                   irs-integration rule shear
%                                   (ir x ir gausspoints)    
%  
%           D                       constitutive matrix for
%                                   plane stress         
%        
%           G                       constitutive matrix Shear (2 x 2)
%
%           eq=[qz]                 load/unit area
%  
%    OUTPUT: Ke :  element stiffness matrix (12 x 12)
%            fe : equivalent nodal forces (12 x 1)
%  -------------------------------------------------------------
%
%   Element dofs: z-disp_1,x-rot_1,y-rot_1,z-disp_2,x-rot_2,....
%   cf. platre.m
%  -------------------------------------------------------------

t=ep(1);
if length(ep)<3
    irb=2;
    irs=1;
else
    irb=ep(2);
    irs=ep(3);
end

ngpb=irb^2;
ngps=irs^2;

format long

Dplate=D.*(t^3/12);
Gt=G.*t;

Bphi=zeros(3,2*4);
Bw=zeros(2,4);
Nphi=zeros(2,2*4);

Ke=zeros(2*4+4,2*4+4);
fe=zeros(2*4+4,1);

phidofs=1:(2*4);
wdofs=2*4+(1:4);

[xiet,gw]=gaussquad(ngps);
for i=1:ngps
    xi=xiet(1,i);
    eta=xiet(2,i);
    
    [N,B]=shapequad(xi,eta,4);
    JT=B*[ex' ey']; 
    JTinv=JT^(-1);
    detJ=det(JT);
    Wt=detJ*gw(i);

    %phi-dofs
    [N,dNdxi]=shapequad(xi,eta,4);
    Nphi(1,1:2:end)=N;
    Nphi(2,2:2:end)=N;
    %w-dofs
    [Nw,dNdxi]=shapequad(xi,eta,4);
    Bw=JTinv*dNdxi;
    
    Ke(phidofs,phidofs)=Ke(phidofs,phidofs)+(Nphi'*Gt*Nphi).*Wt;
    Ke(phidofs,wdofs)=Ke(phidofs,wdofs)-(Nphi'*Gt*Bw).*Wt;
    Ke(wdofs,phidofs)=Ke(wdofs,phidofs)-(Bw'*Gt*Nphi).*Wt;
    Ke(wdofs,wdofs)=Ke(wdofs,wdofs)+(Bw'*Gt*Bw).*Wt;
    if nargout>1
        fe(wdofs)=fe(wdofs)+(q*Wt).*Nw';
    end
end

[xiet,gw]=gaussquad(ngpb);

for i=1:ngpb
    xi=xiet(1,i);
    eta=xiet(2,i);
    
    [N,B]=shapequad(xi,eta,4);
    JT=B*[ex' ey']; 
    JTinv=JT^(-1);
    detJ=det(JT);
    Wt=detJ*gw(i);

    %phi-dofs
    [N,dNdxi]=shapequad(xi,eta,4);
    Nphi(1,1:2:end)=N;
    Nphi(2,2:2:end)=N;
    dNdx=JTinv*dNdxi;
    Bphi(1,1:2:end)=dNdx(1,:);
    Bphi(2,2:2:end)=dNdx(2,:);
    Bphi(3,1:2:end)=dNdx(2,:);
    Bphi(3,2:2:end)=dNdx(1,:);
    
    Ke(phidofs,phidofs)=Ke(phidofs,phidofs)+Bphi'*(Dplate.*Wt)*Bphi;
end

%Transform phi_x,phi_y ->dw/dx,dw/dy 
dwdxdphi=[0 -1;1 0];
R=zeros(12,12);
for i=1:4
    R((i-1)*2+[1 2],(i-1)*2+[1 2])=dwdxdphi;
end
R(9:12,9:12)=eye(4);

fe=R'*fe;

fe = fe( [9 1 2 10 3 4 11 5 6 12 7 8], 1 );
Ke=R'*Ke*R;

Ke=Ke([9 1 2 10 3 4 11 5 6 12 7 8],[9 1 2 10 3 4 11 5 6 12 7 8]);

function [N,B]=shapequad(xi,eta,nn)

if nn==4
    N=[(1-xi)*(1-eta) xi*(1-eta) xi*eta (1-xi)*eta];
    B=[eta-1          1-eta      eta    -eta;
       xi-1           -xi        xi     1-xi];
elseif nn==5
    N=[(1-xi)*(1-eta) xi*(1-eta) xi*eta (1-xi)*eta];
    N(5)=N(1)*N(2)*N(3)*N(4)*256;
    B=[eta-1          1-eta      eta    -eta;
       xi-1           -xi        xi     1-xi];
    B(:,5)=256*(N(1)*N(2)*N(3)*B(:,4)+N(1)*N(2)*N(4)*B(:,3)+N(1)*N(3)*N(4)*B(:,2)+N(2)*N(3)*N(4)*B(:,1));
end

function [xiet,w]=gaussquad(ngauss)

if ngauss==1
    xiet=[1/2;
          1/2];
    w=[1];
elseif ngauss==4
    xiet=([-1 1 1 -1;
           -1 -1 1 1]./sqrt(3)+ones(2,4))./2; 
    w=[1 1 1 1]./4;
elseif ngauss==9
    xiet=([-1 0 1 -1 0 1 -1 0 1;
           -1 -1 -1 0 0 0 1 1 1].*sqrt(3/5)+ones(2,9))./2; 
    w=[25 40 25 40 64 40 25 40 25]./(18)^2;
end