% Project 3: Testing element routine shell2re
% Author: James Koch
%--------------------------------------------------------------------------
% PURPOSE
% Testing that the code written in the element subroutine shell2re
% is correct.
% 
%--------------------------------------------------------------------------
% Copyright (C) 2018 James Koch and Amer Bitar
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License v2 as published by
% the Free Software Foundation.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%--------------------------------------------------------------------------

% define general properties
% geometry
Lx = 3.0;                               % [m], length
Ly = 1.0;                               % [m], width
t = 0.008;                              % [m], thickness

% material constants
E = 210e9;                              % [Pa], modulus of elasticity
nu = 0.3;                               % [-], Poisson's ratio

% external loading
t_y = -5e6;                             % [N/m^2], in-plane load
q_z = -1e3;                             % [N/m^2], out-of-plane load

% create FE-mesh
% number of elements in each direction
% NOTE: choose even number of elements in x- and y-direction to get
% translational DOFs at the center of the shell element
nx = 10;
ny = 10;

% plot the mesh?
plotMesh = 'yes';

% generate the FE-mesh
[Coord, Dof, Enode, Edof, Ex, Ey, ...
 DofRight, DofTop, DofLeft, DofBottom, cdof] = quadmesh(Lx, Ly, nx, ny, ...
							  plotMesh);

% define FE method specific properties
% define constitutive matrices
Dmat = (E/(1 - nu^2))*[1 nu 0;
                       nu 1 0;
                       0 0 (1/2)*(1 - nu)];
Gmat = E/(2*(1 + nu))*[1 0;
                       0 1];

% define element propertiesg                    
ptype = 1;          % 1=plane stress; % 2=plane strain
irb = 2; irs = 2;   % integration rule (only for Mindlin plate)
ep = [ptype t irb irs];

% define external element loading
bx = 0;
by = 0;
qz = q_z;
eq = [bx by qz];

% define solution method acc. to plate theory
plate = 'Kirchoff';

% call element routine
[Ke, fe] = shell2re(Ex(1, :), Ey(1, :), ep, Dmat, Gmat, eq, plate);

%% check mixed plan and plat parts of Ke are zero
assert(Ke(3, 1) == 0, ['correct: mixed plan and plat elements ' ...
                    'in Ke are zero']);
assert(Ke(1, 3) == 0, ['correct: transposed part of first ' ...
                    'check']);

%% check that the in-plane forces equal zero
assert(fe(1) == 0, 'correct: in-plane forces are zero');
