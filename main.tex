\documentclass[12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{enumitem}
\usepackage{listings}
\usepackage{hyperref}
\usepackage{subcaption}
\usepackage{tikz}
\usepackage{amsmath} % allows you to put text in the math environment.
\usepackage{bm}
\usepackage[margin=0.7in]{geometry}
\usepackage{mathtools}
\usepackage{verbatim}
\usepackage{tikz}
\usepackage{tikz-3dplot}
\usetikzlibrary{shapes, calc, positioning}
\tdplotsetmaincoords{70}{120}

\title{\Huge\textbf{Project 3}}
\author{Amer Bitar and James Koch}
\date{\oldstylenums{2019}-\oldstylenums{03}-\oldstylenums{15}}

% -------------------- Graphics Path
\graphicspath{ {./imgs/} }

% -------------------- Indention
\setlength{\parindent}{0cm}
\setlength{\parskip}{\baselineskip}

% -------------------- Colors
\usepackage{color}                     %red & green & blue & yellow & cyan & magenta & black & white
\definecolor{mygreen}{RGB}{28,172,0}   % color values Red & Green & Blue
\definecolor{mylilas}{RGB}{170,55,241}

% -------------------- Define new math command
\newcommand\myeq{\mathrel{\stackrel{\makebox[0pt]{\mbox{\normalfont\tiny def}}}{=}}}
\newcommand\Gauss{\mathrel{\stackrel{\makebox[0pt]{\mbox{\normalfont\tiny Gauss'}}}{=}}}


\begin{document}

\lstset{language=Matlab,%
    %basicstyle=\color{red},
    breaklines=true,                                  %
    morekeywords={matlab2tikz},                       %
    keywordstyle=\color{blue},                        % 
    morekeywords=[2]{1}, keywordstyle=[2]{\color{black}},
    identifierstyle=\color{black},                    %
    stringstyle=\color{mylilas},                      
    commentstyle=\color{mygreen},                     %
    showstringspaces=false,                           % without this there will be a symbol in the places where there is a space
    numbers=left,                                     %
    numberstyle={\tiny \color{black}},                % size of the numbers
    numbersep=9pt,                                    % this defines how far the numbers are from the text
    emph=[1]{for,end,break},emphstyle=[1]\color{red}, %some words to emphasise  
}

% -------------------- Title Page
\maketitle
\begin{center}
  \includegraphics[width=0.9\linewidth]{imgs/z_disp.eps}
\end{center}
\newpage                              % to create a title page

\section{Problem description}
\label{sec:problemdescription}
The problem studied here is that of plate instability since this is of interest to properly design high and thin beams and walls.
In this problem, to determine the influence on the plate stability both in-plane and lateral loading is taken into account as well as different boundary conditions.

The plate is assumed to be very thin compared to the length and width dimensions and is made of a isotropic steel material with a elastic modulus, \(E = 210 GPa\), and Poisson's ratio, \(\nu = 0.3\).
Figure~\ref{fig:problemgeometry} shows the plate geometry used to set up the geometry of the domain upon which the loads are applied.

\begin{figure}[h!]
  \centering
  \includegraphics{imgs/problemgeometry.png}
  \caption{Plate geometry of the problem domain.}
  \label{fig:problemgeometry}
\end{figure}

The loading applied to the plate contains two components which are:
\begin{itemize}
\item a \textit{in-plane} external load, \(t_y = -5 \frac{MN}{m^2}\), and
\item a constant distributed load, \(q_z = -1 \frac{kN}{m^2}\), acting in the z-direction perpendicular to the plane.
\end{itemize}

In the finite element method, the element used will be a rectangular plane shell element which combines the plane stress element with the plate bending element.
For the majority of this project, the Kirchoff model for plates will be used; however, a comparison to the Mindlin model for plates will also be included.
By utilizing the Kirchoff model for plates, the combined element will have 5 degrees of freedom in each node where 3 degrees of freedom relate to displacement and the remaining 2 degrees of freedom relate to rotations.
Next a brief overview of the Kirchoff plate theory is introduced including the strong and weak formulations which are subsequently used in the derivation of the finite element (FE) formulation of our particular problem introduced above.

\section{Background: Kirchoff plate theory}
\label{sec:background}

\subsection{Strong formulation}
\label{sec:backgroundstrong}
To begin the strong and corresponding weak form of the general Kirchoff plate elasticity problem is stated \footnote{In this section, variables designated as vectors in Voigt notation are denoted like \(\vec{\nabla}\) while bolded variables indicate a Voigt notation matrix operator like \(\boldsymbol{\tilde{\nabla}}\). \label{fn:1}}.
The strong form of the Kirchoff plate is stated in equations~\ref{eq:strongform_inplane} and \ref{eq:strongform_bending} for the in-plane and bending problems, respectively.
Of course, to complete the strong form, boundary conditions should be supplied; however, these will be stated later on with the finite element formulation where the problem specific boundary conditions are introduced.
In general, either essential (Dirichlet) or natural (Neumann) boundary conditions should be included with the expression for the strong form. 

\begin{align}
  \text{in-plane equilibrium: } - \vec{\nabla} \vec{N} &= \vec{b}   \label{eq:strongform_inplane} \\
  \begin{split}
      \text{vert. equilibrium: } - \vec{\nabla}^T \vec{V} &= q \\
  \text{moment equilibrium: } \boldsymbol{\tilde{\nabla}} \vec{M} &= \vec{V}   \label{eq:strongform_bending}
  \end{split}
\end{align}

Furthermore, the derivative operators \(\vec{\nabla}\) and \(\boldsymbol{\tilde{\nabla}}\) are defined, in  Voigt notation, as follows:

\begin{equation}
  \label{eq:vecnabla} \nonumber
  \vec{\nabla} =
  \begin{bmatrix}
    \frac{\partial}{\partial x} \\
    \frac{\partial}{\partial y}
  \end{bmatrix}
\end{equation}

\begin{equation}
  \label{eq:matnablatilde} \nonumber
  \boldsymbol{\tilde{\nabla}} =
  \begin{bmatrix}
    \frac{\partial}{\partial x} & 0 \\
    0 & \frac{\partial}{\partial y} \\
    \frac{\partial}{\partial y} & \frac{\partial}{\partial x}
  \end{bmatrix}
\end{equation}

Considering only equations~\ref{eq:strongform_bending}, the weak form of the problem can be derived by multiplying the bending part (Eq.~\ref{eq:strongform_bending}) of the strong form by a weight function, \(\delta w(x, y)\), and integrating by parts twice.
Two integration by parts are required since the out-of-plane external load is proportional to the fourth derivative of the scalar out-of-plane displacement field, \(w(x, y)\), in the governing differential equations.
Therefore, in order to introduce the lowest order FE-approximations it is of interest to shift 2 derivatives of the scalar out-of-plane displacement field, \(w(x, y)\), to the weight function, \(\delta w(x, y)\).
This is important feature for the implementation of a element type in the FE-method because elements should have properties of \underline{completeness} and \underline{compatibility}.

To derive the weak form first the vertical equilibrium condition of the bending problem is used, integration by parts can be performed once and then on subsititution with the moment equilibrium, the integration by parts can be performed a second time.
Additionally, it should be noted that in the derivation of the weak form, generic boundary conditions are introduced which will be translated to the problem specific boundary conditions in the FE-form in section~\ref{sec:t1subtask1}.
Beginning with the vertical equilibrium, the derivation is as follows:

\begin{align}
  \label{eq:weakformstart}
  \int_\Omega (\delta w) \left( \vec{\nabla}^T \vec{V} + q \right) d\Omega + \int_{\Gamma} (\delta w) \left( \vec{V}_n - \vec{n} \vec{V} \right) d\Gamma - \int_{\Gamma} \vec{\nabla}^T (\delta w) \left( \vec{t}_n^M - \mathbf{\tilde{n}} \vec{M} \right) d\Gamma = 0 \\
  \text{, where } \vec{n} \text{ is a normal vector to the components of the shear force} \nonumber \\
  \text{acting on the boundary of the domain and} \nonumber \\
  \mathbf{\tilde{\nabla}} \text{ is a normal vector to the components of the moment} \nonumber \\
  \text{ acting on the boundary of the domain.} \nonumber
\end{align}

Performing integration by parts for the first time: 

\begin{align} \nonumber
  \begin{split}
    \int_\Omega (\delta w) \vec{\nabla}^T \vec{V} d\Omega &= \int_\Omega \vec{\nabla}^T \left( (\delta w) \vec{V} \right) d\Omega - \int_\Omega \vec{\nabla}^T (\delta w) \vec{V} d\Omega \\
    &\Gauss \int_\Gamma (\delta w) \vec{n}^T \vec{V} d\Omega - \int_\Omega \vec{\nabla}^T (\delta w) \vec{V} d\Omega
  \end{split}
\end{align}

Inserting the moment equilibrium from the strong form (Eq.~\ref{eq:strongform_bending}) of \(\vec{V} = \boldsymbol{\tilde{\nabla}} \vec{M}\) into the last term and integrate by parts again.

\begin{align} \nonumber
  \begin{split}
    \int_\Omega (\delta w) \vec{\nabla}^T \vec{V} d\Omega &= \int_\Gamma (\delta w) \vec{n}^T \vec{V} d\Omega - \int_\Omega \vec{\nabla}^T (\delta w) \boldsymbol{\tilde{\nabla}} \vec{M} d\Omega \\
    &= \int_\Gamma (\delta w) \vec{n}^T \vec{V} d\Omega - \int_\Omega \boldsymbol{\tilde{\nabla}}^T \left( \vec{\nabla}^T (\delta w) \vec{M} \right) d\Omega + \int_\Omega \boldsymbol{\tilde{\nabla}} \vec{\nabla} (\delta w) \vec{M} d\Omega \\
    &\text{, where } \boldsymbol{\tilde{\nabla}} \vec{\nabla} = \overset{*}{\vec{\nabla}} \\
    &\Gauss \int_\Gamma (\delta w) \vec{n}^T \vec{V} d\Omega - \int_\Gamma \left( \vec{\nabla}^T (\delta w) \right) \mathbf{\tilde{n}} \vec{M} d\Omega + \int_\Omega \overset{*}{\vec{\nabla}} (\delta w) \vec{M} d\Omega \\
  \end{split}
\end{align}

Here the definition of \(\overset{*}{\vec{\nabla}}\) is:

\begin{equation}
  \label{eq:nablastar} \nonumber
  \overset{*}{\vec{\nabla}} =
  \begin{bmatrix}
    \frac{\partial^2}{\partial x^2} \\
    \frac{\partial^2}{\partial y^2} \\
    \frac{\partial^2}{\partial x \partial y}
  \end{bmatrix}
\end{equation}

Inserting the expression found from integrating by parts twice from above back into the equation~\ref{eq:weakformstart} and simplifying, the derivation of the weak form continues as follows:

\begin{align}
  \begin{split} \nonumber
    - \int_\Omega \overset{*}{\vec{\nabla}} (\delta w) \vec{M} d\Omega &+ \int_\Gamma (\delta w) \vec{n}^T \vec{V} d\Omega - \int_\Gamma \left( \vec{\nabla}^T (\delta w) \right) \mathbf{\tilde{n}} \vec{M} d\Omega\\
    + \int_\Omega (\delta w) q d\Omega &+ \int_{\Gamma} (\delta w) \left( \vec{V_n} - \vec{n} \vec{V} \right) d\Gamma - \int_{\Gamma} \vec{\nabla}^T (\delta w) \left( \vec{t}_n^M - \mathbf{\tilde{n}} \vec{M} \right) d\Gamma = 0
  \end{split} \\
  \begin{split} \label{eq:weakformsimplified}
    - \int_\Omega \overset{*}{\vec{\nabla}} (\delta w) \vec{M} d\Omega &+ \int_\Omega (\delta w) q d\Omega + \int_{\Gamma} (\delta w) \vec{V_n} d\Gamma - \int_{\Gamma} \vec{\nabla}^T (\delta w) \vec{t}_n^M d\Gamma = 0
  \end{split}
\end{align}

The last term in the equation~\ref{eq:weakformsimplified} is now considered by splitting the traction moment into normal and tangential components with respect to the boundary surface as follows\footnote{\(\mathbf{\tilde{n}}\) is defined similarily to \(\boldsymbol{\tilde{\nabla}}\) Voigt notation matrix except the rows correspond to the normal vectors in x, y, and xy cross-plane.}:

\begin{align}
  \begin{split} \nonumber
    \vec{t}_n^M = \mathbf{\tilde{n}}^T \vec{M} &= M_{nn} \vec{n} + M_{nm} \vec{m} \\
    \int_{\Gamma} \vec{\nabla}^T (\delta w) \vec{t}_n^M d\Gamma &= \int_{\Gamma} \vec{\nabla}^T (\delta w) \vec{n} M_{nn} d\Gamma + \int_{\Gamma} \vec{\nabla}^T (\delta w) \vec{m} M_{nm} d\Gamma \\
    \int_{\Gamma} \vec{\nabla}^T (\delta w) \vec{t}_n^M d\Gamma &= \int_{\Gamma} \frac{\delta (\delta w)}{\delta n} \vec{n} M_{nn} d\Gamma + \int_{\Gamma} \frac{\delta (\delta w)}{\delta m} \vec{m} M_{nm} d\Gamma \\
  \end{split}
\end{align}

From the integral of the tangential moment, \(M_{nm}\), (2nd term of the above equation), it is clear to see that this integral will be evaluated to zero since it is a integral over a closed loop \footnote{The mathematical proof of this is not shown here. \label{fn:2}}.
The remaining term from the above equation is defined as the ``Kirchoff shear force'' and relates to the normal moment, \(M_{nn}\).
The two components just described are shown mathematically as follows:

\begin{align} 
  \begin{split} \nonumber
    \int_{\Gamma} \frac{\delta (\delta w)}{\delta m} \vec{m} M_{nm} d\Gamma &= \int_{\Gamma} \frac{\delta (\delta w)}{\delta m} \vec{m} M_{nm} dm \\
    &= \underbrace{ \int_{\Gamma} \frac{\delta}{\delta m} (\delta w M_{nm}) d\Gamma }_{= 0} - \underbrace{ \int_{\Gamma} \delta w \frac{\delta M_{nm}}{\delta m} d\Gamma }_{\text{``Kirchoff shear force''}}
  \end{split}
\end{align}

Therefore, the weak form is shown in equation~\ref{eq:weakformwithmoment}.

\begin{align}
  \label{eq:weakformwithmoment}
  - \int_\Omega \overset{*}{\vec{\nabla}} (\delta w) \vec{M} d\Omega + \int_\Omega (\delta w) q d\Omega &+ \int_{\Gamma} (\delta w) \vec{V}_n^K d\Gamma - \int_{\Gamma} \frac{\delta (\delta w)}{\delta n} M_{nn} d\Gamma = 0 \text{, where } \\
  &\vec{V}_n^K = V_n + \frac{\delta M_{nm}}{\delta m} \nonumber
\end{align}

Implementing the constitutive relationship between the moment and curvature is shown in equation~\ref{eq:constitutiverelationship}.

\begin{align}
  \label{eq:constitutiverelationship}
  \begin{split}
    \vec{\kappa}[w] &= \overset{*}{\vec{\nabla}} w \\
    \vec{M} &= - \mathbf{\tilde{D}} \vec{\kappa}(w) = - \mathbf{\tilde{D}} \overset{*}{\vec{\nabla}} w
  \end{split}
\end{align}

The final derivation of the weak form of the Kirchoff plate is shown in equation~\ref{eq:weakform} again without explicitly stating boundary conditions (only generic boundary condition terms are present) which are necessary but will be introduced with the FE formulation in the next section~\ref{sec:methoddescription}.
Also, the parameter, \(\mathbf{\tilde{D}}\), will be defined in section~\ref{sec:t1subtask1} in FE-formulation.

\begin{align}
  \label{eq:weakform}
    \int_\Omega \vec{\kappa}[\delta w] \mathbf{\tilde{D}} \vec{\kappa} [w] d\Omega + \int_\Omega (\delta w) q d\Omega &+ \int_{\Gamma} (\delta w) \vec{V}_n^K d\Gamma - \int_{\Gamma} \frac{\delta (\delta w)}{\delta n} M_{nn} d\Gamma = 0 
\end{align}

\section{Method description}
\label{sec:methoddescription}

\subsection{Stress analysis}
\label{sec:stressanalysis}

\subsubsection{Finite element formulation}
\label{sec:t1subtask1}

The FE-form will now be derived with respect to the specific plate bending problem stated in section~\ref{sec:problemdescription}.
Both the FE-form of the in-plane and bending problem are shown here beginning with the FE-form of the in-plane problem\footnote{In this section, variables designated as vectors or matrices are denoted using boldface. The vectors and matrices in this section are defined in the classical linear algebra sense which is different than in the previous section where vector and matrix notation was used to differentiate between vectors and tensors using Voigt notation. \label{fn:3}}.
As an aside, Figure~\ref{fig:boundarydef} introduces the naming convention for the 4 boundaries of the plate domain for future reference to in terms of the applied essential and natural boundary conditions.

\begin{figure}[h!]
  \centering
  \begin{tikzpicture}
    \draw (0,0) -- (3,0) node[pos=0.5, below]{\(\Gamma_3\)};
    \draw (3,0) -- (3,1) node[pos=0.5, right]{\(\Gamma_4\)};
    \draw (3,1) -- (0,1) node[pos=0.5, above]{\(\Gamma_1\)};
    \draw (0,1) -- (0,0) node[pos=0.5, left]{\(\Gamma_2\)};
  \end{tikzpicture}
  \caption{Naming scheme on the boundaries of a 2D plate domain.}
  \label{fig:boundarydef}
\end{figure}

First, the FE-form of the \textbf{in-plane problem} is stated in equation~\ref{eq:FEform_inplane_integralform}.
Since the in-plane problem reduces simply to a 2D linear elasticity problem the FE-form is simple to state.

\begin{align}
  \label{eq:FEform_inplane_integralform}
  \mathbf{c}^T \int_\Omega \mathbf{B}^T \mathbf{D} \mathbf{B} d\Omega \mathbf{a} &= \mathbf{c}^T \left[ \int_\Omega \mathbf{N}^T \mathbf{b} d\Omega + \int_{\Gamma_{g}} \mathbf{N}^T \mathbf{t} t d\Gamma + \int_{\Gamma_{h}} \mathbf{N}^T h t d\Gamma \right]
\end{align}

The matrices and vectors representing the shape functions and the nodal values are shown later in conjunction with the bending problem since the same shape functions and mesh are used for both problems.
In the integral above, the natural boundary condition, \(h\), is defined as the known traction load applied on the natural boundary, \(\Gamma_h\) while \(\mathbf{t}\) is the reaction traction load which results from the application of essential boundary conditions on the part of the boundary, \(\Gamma_g\).
Additionally, for 2D linear elasticity of isotropic materials the stiffness matrix, \(\mathbf{D}\) is defined as follows:

\begin{equation}
  \label{eq:stiffnessfor2Dlinearelasticity}
  \mathbf{D} = \frac{E}{(1 - \nu^2)}
  \begin{bmatrix}
    1 & \nu & 0 \\
    \nu & 1 & 0 \\
    0 & 0 & \frac{1}{2} (1 - \nu)
  \end{bmatrix}
\end{equation}

The system of equations form is then written as follows in equation~\ref{eq:FEform_inplane}.

\begin{align}
  \label{eq:FEform_inplane} 
  &\mathbf{K} \mathbf{a} = \mathbf{f}_l + \mathbf{f}_b \\
  &\begin{cases} \nonumber
    u_x &= 0 \text{ on } \Gamma_{g,2} \text{ and } \Gamma_{g,4} \\
    u_y &= 0 \text{ on } \Gamma_{g,2} \text{ and } \Gamma_{g,4} \\
    t_y &= -5000 \frac{kN}{m^2} \text{ on } \Gamma_{h,1}
  \end{cases}\\
  &\text{, where } \Gamma_{g,2} \text{ and } \Gamma_{g,4} \text{ are the right and left side boundaries of the plate and } \nonumber \\
  &\Gamma_{h,2} \text{ is the top boundary.} \nonumber
\end{align}

Secondly, the FE-form of the \textbf{bending problem} is stated in equation~\ref{eq:FEform_bending} and the variables will be defined subsequently.

\begin{align}
  \label{eq:FEform_bending_integralform}
  \begin{split}
    \delta \mathbf{a}^T \int_\Omega \overset{*}{\mathbf{B}}^T \mathbf{\tilde{D}} \overset{*}{\mathbf{B}} d\Omega \mathbf{a} &= \delta \mathbf{a}^T \left[ \int_\Omega \mathbf{N}^T q d\Omega + \int_{h,I} \mathbf{N}^T h_I d\Gamma - \int_{h,II} \mathbf{B}^T \mathbf{n} h_{II} d\Gamma \right] \\
    &+ \delta \mathbf{a}^T \left[ \int_{g,I} \mathbf{N}^T \mathbf{V}_n^K d\Gamma - \int_{\Gamma_{g,II}} \mathbf{B}^T \mathbf{n} \mathbf{M}_{nn} d\Gamma \right] \\
  \end{split}
\end{align}

\begin{align}
  \label{eq:FEform_bending}
  &\mathbf{K} \mathbf{a} = \mathbf{f}_l + \mathbf{f}_b \\
  &\begin{cases} \nonumber
    w(x, y) &= 0 \text{ on } \Gamma_{g,1}, \Gamma_{g,2}, \Gamma_{g,3} \text{, and } \Gamma_{g,4} \text{, where} \\ 
    &w(x, y) \text{is the out-of-plane displacements of the plate and} \\
    &\text{the four boundaries, } \Gamma_{g,i} \text{ correspond to all edges of the plate.}
  \end{cases}
\end{align}

In the bending problem, the variable of interest is the out-of-plane deflection, \(w(x, y)\), which is defined as a scalar 2D field and therefore, the FE approximation begins with approximating this scalar field with shape functions, \(N_i\), and nodal values, \(a_j\), where \(i\) denotes the number of nodes per element and \(j\) denotes the value of the deflection in one degree of freedom.
As usual in the FE-method, we also define the approximation for the weight function, \(\delta w\), in terms of the same shape functions, \(N_i\), and aribtrary coefficients, \((\delta a)_j\).
The primary FE-approximations are stated as follow:

\begin{align} 
  \label{eq:FEapproximations}
  w(x, y) &= \mathbf{N} \mathbf{a} \text{, where} \\
  \delta w(x,y) &= \mathbf{N} \mathbf{\delta a} = \mathbf{\delta a}^T \mathbf{N}^T
\end{align}

\begin{equation}
  \mathbf{N} =
  \begin{bmatrix}
    N_1 \\
    N_2 \\
    N_3 \\
    \vdots \\
    N_N
  \end{bmatrix}
\end{equation}

\begin{equation}
  \mathbf{a} =
  \begin{bmatrix}
    u_{1,x} \\
    u_{1,y} \\
    u_{1,z} \\
    \theta_{1,x} \\
    \theta_{1,y} \\
    \vdots \\
    u_{Ndof,x} \\
    u_{Ndof,y} \\
    u_{Ndof,z} \\
    \theta_{Ndof,x} \\
    \theta_{Ndof,y}
  \end{bmatrix}
\end{equation}

Here it should be noted that the number of shape functions, \(N_N\), is equal to the number of nodes within each element defined in the domain of the problem which in this problem is defined as \(4\) since rectangular 4-node elements are used.
Additionally, \(Ndof\) is defined as the total number of degrees of freedom in the global domain.

The curvature and slope of the plate under bending is then defined in terms of the FE-approximation as follows\footnote{The vector notation like \(\vec{\kappa}\) is used to refer to the curvature in the Voigt notation and is not strictly a vector in the linear algebra sense. \label{fn:4}}:

\begin{align}
  \begin{split} \nonumber
    \vec{\kappa} &= \overset{*}{\mathbf{B}} \mathbf{a} \\
    \frac{\delta (\delta w)}{\delta n} &= \vec{n}^T \vec{\nabla}^T (\delta w) = \mathbf{n}^T \mathbf{B} \mathbf{\delta a} = \mathbf{\delta a}^T \mathbf{B}^T \mathbf{n}
  \end{split}
\end{align}

The normal direction, \(\vec{n}\)\footnote{see previous footnote~\ref{fn:4} \label{fn:5}}, is defined simply as follows.

\begin{equation} \nonumber
  \mathbf{n} =
  \begin{bmatrix}
    n_x & 0
  \end{bmatrix}
\end{equation}

Furthermore, both the first and second derivatives of the shape functions are required in the FE-form and are defined as follows.

\begin{equation} \nonumber
  \mathbf{B} =
  \begin{bmatrix}
    \frac{\partial N_1}{\partial x} & \frac{\partial N_2}{\partial x} & ... & \frac{\partial N_{Ndof}}{\partial x} \\
    \frac{\partial N_1}{\partial y} & \frac{\partial N_2}{\partial y} & ... & \frac{\partial N_{Ndof}}{\partial y}
  \end{bmatrix}
\end{equation}

\begin{equation} \nonumber
  \overset{*}{\mathbf{B}} =
                            \begin{bmatrix}
                              \frac{\partial^2 N_1}{\partial x^2} & \frac{\partial^2 N_2}{\partial x^2} & ... & \frac{\partial^2 N_{Ndof}}{\partial x^2} \\
                                \frac{\partial^2 N_1}{\partial y^2} & \frac{\partial^2 N_2}{\partial y^2} & ... & \frac{\partial^2 N_{Ndof}}{\partial y^2} \\
                                  2 \frac{\partial^2 N_1}{\partial x \partial y} & 2 \frac{\partial^2 N_2}{\partial x \partial y} & ... & \frac{\partial^2 N_{Ndof}}{\partial x \partial y}          
                                  \end{bmatrix}
\end{equation}

The plate bending stiffness is then determined based on linear elastic stiffness of a isotropic material as follows:

\begin{align}
  \label{eq:platebendingstiffness}
  \mathbf{\tilde{D}} &= \frac{t^3}{12} \mathbf{D}
\end{align}

The next two sections (Sec.~\ref{sec:elementsubroutine1} and \ref{sec:elementsubroutine2}) will introduce the MATLAB implementation for the plate problem by combining the in-plane and out-of-plane (i.e. bending) problem into one system of equations, \(\mathbf{K}\mathbf{a} = \mathbf{f}\), as well as computing the stresses, strains, sectional forces, and curvature for all nodes in each element.

\subsubsection{Element subroutine for a plane shell element}
\label{sec:elementsubroutine1}
The stiffness matrix and load vector for the combined in-plane and out-of-plane problems can be computed by combining the degrees of freedom (DOF) of the in-plane displacements (from the in-plane problem) with the DOF of out-of-plane displacements and rotations (from the bending problem).
This is done by combining the different element types which are best used to solve each problem seperately which are:

\begin{enumerate}
\item 2D planar element (in-plane problem) to solve for \(u_x\) and \(u_y\)
\begin{lstlisting}
% in-plane contribution
[Ke_plane, fe_plane]=planre(ex_plane, ey_plane, ...
                            ep(1:2), D, eq(1:2)');
\end{lstlisting}
\item 2D plate element (bending problem) to solve for \(w(x, y)\), \(\theta_x\), and \(\theta_y\)
\begin{lstlisting}
% bending contribution
if strcmp(plate, 'Kirchoff')
    [Ke_plate, fe_plate] = platre(ex, ey, ep(2), D, eq(3));
elseif strcmp(plate, 'Mindlin')
    [Ke_plate, fe_plate] = platme(ex, ey, ep(2:4), D, G, eq(3));
end
\end{lstlisting}
\end{enumerate}

This is preformed by implementing the element routine \lstinline{shell2re} in MATLAB which uses the CALFEM functions \lstinline{planre} and \lstinline{platre} to compute the contributions to the element stiffness matrix from the in-plane and bending problem.
Care must be taken to ensure consistent ordering of the DOFs in the combined problem when assembling together the planar and plate element stiffness matrices into the combined element stiffness matrix.
The implementaton used in this report is that the DOF are listed in the following order starting from the bottom leftmost node within each element and continuing counterclockwise:

\begin{equation}
  \begin{bmatrix} \nonumber
    u_{i,x} & u_{i,y} & u_{i,z} & \theta_{i,x} & \theta_{i,y} & \ldots
  \end{bmatrix}
\end{equation}

This then means for a element with 4 nodes, there will exist 20 DOF per element numbered from 1 to 20.
Thus, the implementation in MATLAB to identify the rows and columns of the contribution from the planar (in-plane) and plate (bending) stiffness can be accomplished in the following code:

\begin{lstlisting}
% define in-plane degrees of freedom
v1 = 1:5:20;
v2 = 2:5:20;
N = max(numel(v1), numel(v2));
ia = 1+rem(0:N-1, numel(v1));
ib = 1+rem(0:N-1, numel(v2));
plane_ind = [v1(ia); v2(ib)];
plane_ind = reshape(plane_ind,1,[]);

% define bending degrees of freedom
v3 = 3:5:20;
v4 = 4:5:20;
v5 = 5:5:20;
N = 4;
ia = 1+rem(0:N-1, numel(v3));
ib = 1+rem(0:N-1, numel(v4));
ic = 1+rem(0:N-1, numel(v5));
plate_ind = [v3(ia); v4(ib); v5(ic)];
plate_ind = reshape(plate_ind,1,[]);
\end{lstlisting}

These indices, \lstinline{plane_ind} and \lstinline{plate_ind}, can then be utilized to assemble the two contributions into the full element stiffness matrix of size 20 by 20 for the combined planar and plate element.
The pertinent lines of code are shown as follows and for the full function can be seen in Appendix~\ref{sec:app-1}.

\begin{lstlisting}
Ke(plane_ind, plane_ind) = Ke_plane;
Ke(plate_ind, plate_ind) = Ke_plate;
\end{lstlisting}

\subsection{Element subroutine to compute stress, strains, sectional forces, and curvature}
\label{sec:elementsubroutine2}
The stresses, strains, sectional forces, and curvature are computed using the \lstinline{planrs} and \lstinline{platrs} functions from the CALFEM library which correspond to the in-plane and bending problems.
The function \lstinline{shell2rs} is written in a similar manner to \lstinline{shell2re} except here the stresses, strains, sectional forces, and curvatures are computed.
As in \lstinline{shell2re}, it is necessary to use the planar, \lstinline{plane_ind}, and plate, \lstinline{plate_ind}, vectors defined above to use the correct displacements (calculated from \lstinline{shell2re}) to determine the in-plane and bending contributions to each parameter, respectively.
The following lines of code in \lstinline{shell2rs} are the most important differences from \lstinline{shell2re}:

\begin{lstlisting}
% the order of the plane strains in the element total stains matrix
ed_plan = ed(plane_ind);  

% the order of the plate strains in the element total stains matrix
ed_plat = ed(plate_ind);

% compute element stresses and strains
% es = [ sigx sigy [sigz] tauxy    element stress matrix
%                ......                ]   one row for each element
% et = [ epsx epsy [gamz] gamxy    element strain matrix
%          ......              ]   one row for each element
[es, et] = planrs(ex_plane, ey_plane, ep(1:2), D, ed_plan);

% compute element forces and curvatures
% es = [ Mxx Myy Mxy Vxz Vyz;   element force matrix
%                    ......          ]  one row for each element
%         et = [kxx,kyy,kxy]       curvature in global coordinates
[ef, ec] = platrs(ex, ey, ep(2), D, ed_plat);
\end{lstlisting}

\section{Results}
\label{sec:results}

\subsection{Stress analysis}
\label{sec:resutlsstressanalysis}
The results from the FE-implementation in MATLAB are shown in terms of the displacements/rotations in each DOF over the entire domain and the moments/stresses in both spatial dimensions (including the twisting moment, \(M_{xy}\), and the shear stress, \(\sigma_{xy}\)) in Figures~\ref{fig:displacements}, \ref{fig:rotations}, \ref{fig:moments}, and \ref{fig:stress}.

\begin{figure}[h!]
  \begin{subfigure}{\linewidth}
    \centering
    \includegraphics[width=0.9\linewidth]{imgs/x_disp.eps}
  \end{subfigure}
  \begin{subfigure}{\linewidth}
    \centering
    \includegraphics[width=0.9\linewidth]{imgs/y_disp.eps}
  \end{subfigure}
  \begin{subfigure}{\linewidth}
    \centering
    \includegraphics[width=0.9\linewidth]{imgs/z_disp.eps}
  \end{subfigure}
  \caption{Translational deformations of a 2D Kirchoff ``thin'' plate.}
  \label{fig:displacements}
\end{figure}

\begin{figure}[h!]
  \begin{subfigure}{\linewidth}
    \centering
    \includegraphics[width=\linewidth]{imgs/x_rot.eps}
  \end{subfigure}
  \begin{subfigure}{\linewidth}
    \centering
    \includegraphics[width=\linewidth]{imgs/y_rot.eps}
  \end{subfigure}
  \caption{Rotational deformations of a 2D Kirchoff ``thin'' plate.}
  \label{fig:rotations}
\end{figure}

\begin{figure}[h!]
  \begin{subfigure}{\linewidth}
    \centering
    \includegraphics[width=0.9\linewidth]{imgs/M_{xx}.eps}
  \end{subfigure}
  \begin{subfigure}{\linewidth}
    \centering
    \includegraphics[width=0.9\linewidth]{imgs/M_{yy}.eps}
  \end{subfigure}
  \begin{subfigure}{\linewidth}
    \centering
    \includegraphics[width=0.9\linewidth]{imgs/M_{xy}.eps}
  \end{subfigure}
  \caption{Moments within a 2D Kirchoff ``thin'' plate.}
  \label{fig:moments}
\end{figure}

\begin{figure}[h!]
  \begin{subfigure}{\linewidth}
    \centering
    \includegraphics[width=0.9\linewidth]{imgs/sig_xx.eps}
  \end{subfigure}
  \begin{subfigure}{\linewidth}
    \centering
    \includegraphics[width=0.9\linewidth]{imgs/sig_yy.eps}
  \end{subfigure}
  \begin{subfigure}{\linewidth}
    \centering
    \includegraphics[width=0.9\linewidth]{imgs/sig_xy.eps}
  \end{subfigure}
  \caption{Stresses within a 2D Kirchoff ``thin'' plate.}
  \label{fig:stress}
\end{figure}

The results in comparing the out-of-plane deflection based on the FE model using Kirchoff and Mindlin plate theory is compared in Figure~\ref{fig:kirchoffvmindlin} as a function of varying plate thickness.
Two curves for the Mindlin plate are computed which correspond to using 2x2 and 1x1 Gauss integration for the shear part.
Section~\ref{sec:discussion} will discuss in detail the differences between the Kirchoff and Mindlin plate theories in particular the phenomenon known as shear locking.

\begin{figure}[h!]
  \centering
  \includegraphics[width=0.9\linewidth]{imgs/kirchoffvmindlin.eps}
  \caption{Comparison of Kirchoff and Mindlin plate theories in terms of the out-of-plane deflection of the center-most point as a function of varying plate thicknesses.}
  \label{fig:kirchoffvmindlin}
\end{figure}

\subsection{Buckling analysis}
\label{sec:resultsbucklinganalysis}
The results for the linearized pre-buckling problem for the first two buckled mode shape which correspond to the lowest two eigenvalues are shown in Figure~\ref{fig:buckledmode}.

\begin{figure}[h!]
    \begin{subfigure}{\linewidth}
    \centering
    \includegraphics[scale=0.5]{imgs/firstbuckledmode.eps}
    \end{subfigure}
    \begin{subfigure}{\linewidth}
    \centering
    \includegraphics[scale=0.5]{imgs/secondbuckledmode.eps}
    \end{subfigure}
    \caption{Buckled modes of the linearized pre-buckling problem.}
    \label{fig:buckledmode}
\end{figure}

The eigenvalues, \(\lambda\), and corresponding critical load are:
\begin{itemize}
    \item Critical eigenvalue: 4.773 \(\rightarrow -2.387 \times 10^7 N\)
    \item 2nd eigenvalue: 6.859 \(\rightarrow -3.430 \times 10^7 N\)
    \item 3rd eigenvalue: 8.852 \(\rightarrow -4.426 \times 10^7 N\)
    \item 4th eigenvalue: 10.354 \(\rightarrow -5.177 \times 10^7 N\)
    \item 5th eigenvalue: 11.970 \(\rightarrow -5.985 \times 10^7 N\)
    \item 6th eigenvalue: 14.048 \(\rightarrow -7.024\times 10^7 N\)
\end{itemize}

\section{Discussion}
\label{sec:discussion}

\subsection{Stress analysis}
\label{sec:discusspart1}
The deflections, moments, and stresses shown in Figures~\ref{fig:displacements} to \ref{fig:stress} are reasonable in relation to the applied boundary conditions.

\subsubsection{Choice of scaling factor for different plate thickness under Kirchoff plate theory}
\label{sec:t2subtask2}
The choice of scaling factor is used to facilitate the comparison between Kirchoff and Mindlin plate theories in Figure~\ref{fig:kirchoffvmindlin} and described in detail in the next section~\ref{sec:t2subtask3}.
The scaling factor used is \(t^3\) since the stiffness of the plate is proportional to the inverse of the thickness cubed.
This is best illustrated using the analogy of a simple Euler-Bernoulli beam as follows:

\begin{align}
    EI \frac{d^4w}{dx^4} &= q \text{ for a simply-supported uniformly distributed loaded beam, where} \\
    \text{the deflection is } w &= \frac{5qL^4}{384EI} \nonumber
\end{align}

Here it is clearly evident that the deflection is proportional to \(\frac{1}{t^3}\).
This can be then applied to plates where the stiffness of the plate is also proportional to \(t^3\) which then implies that the deflection is proportional to the inverse of \(t^3\).

\begin{align}
    \mathbf{\tilde{D}} = \frac{t^3}{12} \mathbf{D}
\end{align}

\subsubsection{Comparison between Kirchoff and Mindlin plate theories}
\label{sec:t2subtask3}
As it is shown in Figure~\ref{fig:kirchoffvmindlin}, in the thin plate the Mindlin theory with 2 integration points gives apparently less deflection when comparing with the Kirchoff theory. 
This is what we call shear locking. 
This shear locking gives extra stiffness to the thin plates (i.e. small thickness), whereas in using one integration point for Mindlin plate theory this stiffness is reduced or has less affect to the shear or the rotation, which is more consistent with plate with small thickness since these plates are more governed by bending than shear.

The basis of shear locking arrives from the difference between how the two theories treat the shear stress.
Similar to Euler-Bernoulli beam thory, Kirchoff's plate theory makes the following two assumptions:
\begin{enumerate}
    \item plane sections remain plane, and
    \item plane sections remain perpendicular to the neutral axis (under bending).
\end{enumerate}

The result of this forces the shear stress in Kirchoff's theory to be zero since the rotational degrees of freedom, \(\theta_x\) and \(\theta_y\), are set equal to the slope of the out-of-plane deflection, \(\frac{\partial w}{\partial x}\) and \(\frac{\partial w}{\partial y}\).
In comparison with Mindlin plate theory (analagous to Timoshenko beam theory), this coupling of rotation and out-of-plane displacement is no longer valid (i.e. the second Kirchoff plate theory assumption no longer holds).

Therefore, Kirchoff plate theory is very effective in the solution of thin plates and should not be applied to thick plates as it underestimates the deflection by not taking into account shear deformations.
For thick plates, Mindlin plate theory should be used since it accounts for shear deformations.
The Mindlin plate theory is also valid for thin plates except care must be taken to avoid shear locking as described above.
The simple remedy to avoid shear locking of thin plates using Mindlin plate theory is to use reduced Gauss integration with respect to shear in the element routine, \lstinline{platme}. 

\subsection{Buckling analysis}
\label{sec:discusspart2}
The buckling analysis preformed here is that of a linearized pre-buckling eigenvalue problem.
This simplifies the buckling analysis by only computing the critical buckling load from where buckling would occur (i.e. the bifurcation point).

As shown above in results (section~\ref{sec:resultsbucklinganalysis}), the two lowest buckling loads are \(2.387 \times 10^7\) and \(3.430 \times 10^7\) Newtons.
The critical buckling mode is the one corresponding to the lowest eigenvalue (lowest load) and it's corresponding eigenvector (buckled shape) shown in Figure~\ref{fig:buckledmode}.

From the FE-form we could see that the in-plane forces contribute in a negative way to the bending stiffness in terms of the second order effects (i.e. as the second order effects increase and reach a critical value the bending stiffness becomes zero and buckling commences). 
In other word, the in-plane force is the main cause for the buckling and the out of-plane force (i.e \(q_z\)) has no contribution in the critical buckling load but it effects the buckling shape. 

\section{Appendix 1: MATLAB m-files}
\label{sec:app-1}

\subsection{Main script}
\label{sec:mainscript}

\lstinputlisting[language=Matlab]{src/CA3.m}

\subsubsection{Function to solve FE problem for comparison between Kirchoff and Mindlin plate theories}
\label{sec:functionsolve}

\lstinputlisting[language=Matlab]{src/solve_2Dplate.m}

\subsection{Stress analysis: Element subroutine \lstinline{shell2re}}
\label{sec:shell2re}

\lstinputlisting[language=Matlab]{src/shell2re.m}

\subsection{Stress analysis: Element subroutine \lstinline{shell2rs}}
\label{sec:shell2rs}

\lstinputlisting[language=Matlab]{src/shell2rs.m}

\subsection{Buckling analysis: Element subroutine \lstinline{shell2rg}}
\label{sec:shell2rg}

\lstinputlisting[language=Matlab]{src/shell2rg.m}

\end{document}
